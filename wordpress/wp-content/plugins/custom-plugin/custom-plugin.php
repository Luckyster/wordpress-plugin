<?php

/**
 * @package Custom Plugin
 */
/*
    Plugin Name: Custom Plugin
    Description: Custom Plugin
    Version: 1.0
    Author: Gleb
    License: GPLv2 or later
    Text Domain: custom-plugin
*/

if ( ! defined( 'ABSPATH' ) ) {
	die;
}
if ( ! class_exists( 'CustomPlugin' ) ) {
	class CustomPlugin {

		public $plugin;

		function __construct() {
			$this->plugin = plugin_basename( __FILE__ );
		}

		function register() {
			add_action( 'admin_enqueue_scripts', array( 'CustomPlugin', 'enqueue' ) );

			add_action( 'admin_menu', array( $this, 'add_admin_pages' ) );

			add_filter( "plugin_action_links_$this->plugin", array( $this, 'settings_link' ) );
		}

		public function settings_link( $links ) {
			$settings_link = '<a href="admin.php?page=custom_plugin">'. __("Settings", "custom-plugin") . '</a>';
			array_push($links, $settings_link);
			return $links;
		}

		public function add_admin_pages() {
			add_menu_page( 'Custom Plugin', 'Custom Plugin', 'manage_options', 'custom_plugin', array( $this, 'admin_index' ) );
		}

		public function admin_index() {
			// require template
			require_once plugin_dir_path( __FILE__ ) . 'templates/admin.php';
		}

		protected function create_post_type() {
			add_action( 'init', array( $this, 'custom_post_type' ) );
		}

		function activate() {
			require_once plugin_dir_path( __FILE__ ) . 'inc/plugin-activate.php';
			CustomPluginActivate::activate();
		}

		function deactivate() {
			require_once plugin_dir_path( __FILE__ ) . 'inc/plugin-deactivate.php';
			CustomPluginDeactivate::deactivate();
		}

		function uninstall() {
		}

		function custom_post_type() {
			register_post_type( 'book', [ 'public' => true, 'label' => 'Books' ] );
		}

		public static function enqueue() {
			wp_enqueue_style( 'mypluginstyle', plugins_url( 'assets/style.css', __FILE__ ) );
			wp_enqueue_script( 'mypluginscript', plugins_url( 'assets/custom.js', __FILE__ ) );

		}
	}


	$custom_plugin = new CustomPlugin();
	$custom_plugin->register();

//activation
	register_activation_hook( __FILE__, array( $custom_plugin, 'activate' ) );
//deactivation
	register_deactivation_hook( __FILE__, array( $custom_plugin, 'deactivate' ) );

}

