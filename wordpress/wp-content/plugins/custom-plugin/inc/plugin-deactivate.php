<?php

/**
 * @package Custom Plugin
 */

class CustomPluginDeactivate {
	public static function deactivate() {
		flush_rewrite_rules();
	}
}